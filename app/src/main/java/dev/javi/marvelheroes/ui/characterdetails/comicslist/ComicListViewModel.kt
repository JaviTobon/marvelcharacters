package dev.javi.marvelheroes.ui.characterdetails.comicslist

import androidx.lifecycle.ViewModel
import dev.javi.marvelheroes.data.MarvelRepository

class ComicListViewModel(repository: MarvelRepository, id: Int) : ViewModel() {
    val comicList = repository.searchComics(id)
}