package dev.javi.marvelheroes

import android.app.Application
import dev.javi.marvelheroes.di.MarvelModule
import org.koin.android.ext.android.startKoin
import org.koin.android.logger.AndroidLogger

@Suppress("unused")
class MarvelApplication : Application() {
    override fun onCreate() {
        super.onCreate()

        startKoin(this, listOf(MarvelModule), logger = AndroidLogger())
    }
}