package dev.javi.marvelheroes

import com.bumptech.glide.annotation.GlideModule
import com.bumptech.glide.module.AppGlideModule

@GlideModule
class MarvelAppGlideModule : AppGlideModule()