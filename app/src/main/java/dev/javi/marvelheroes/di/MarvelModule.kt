package dev.javi.marvelheroes.di

import dev.javi.marvelheroes.data.MarvelCallback
import dev.javi.marvelheroes.ui.mainlist.CharactersListViewModel
import dev.javi.marvelheroes.ui.characterdetails.CharacterDetailsViewModel
import dev.javi.marvelheroes.ui.characterdetails.comicslist.ComicListViewModel
import dev.javi.marvelheroes.ui.characterdetails.eventslist.EventListViewModel
import dev.javi.marvelheroes.data.MarvelRepository
import dev.javi.marvelheroes.data.db.MarvelDatabase
import dev.javi.marvelheroes.data.db.MarvelLocalCache
import dev.javi.marvelheroes.data.network.MarvelApi
import org.koin.android.ext.koin.androidContext
import org.koin.android.viewmodel.ext.koin.viewModel
import org.koin.dsl.module.module

val MarvelModule = module {

    single { MarvelApi.create() }

    single { MarvelDatabase.getInstance(androidContext()).marvelDao()}

    single { MarvelLocalCache(get())}

    single { MarvelRepository(get(), get()) }

    factory { MarvelCallback(getProperty("name"),get(),get())}

    viewModel { CharactersListViewModel(get()) }

    viewModel { CharacterDetailsViewModel(get(), getProperty("id")) }

    viewModel { ComicListViewModel(get(), getProperty("id")) }

    viewModel { EventListViewModel(get(), getProperty("id")) }
}