package dev.javi.marvelheroes

import dev.javi.marvelheroes.models.Character
import dev.javi.marvelheroes.models.CharacterDatabase
import dev.javi.marvelheroes.models.ComicSummary
import dev.javi.marvelheroes.models.EventSummary
import java.security.MessageDigest

fun String.md5(): String {
    val md = MessageDigest.getInstance("MD5")
    val digest = md.digest(toByteArray())
    return digest.joinToString("") {
        String.format("%02x", it)
    }
}

fun List<Character>.toCharacterDatabaseList(): List<CharacterDatabase> {
    val charactersDatabase = mutableListOf<CharacterDatabase>()
    this.forEach {
        charactersDatabase.add(it.toCharacterDatabase())
    }
    return charactersDatabase
}

fun List<ComicSummary>.getComicListWithId(characterId: Int): List<ComicSummary> {
    val comicsWithId = mutableListOf<ComicSummary>()
    this.forEach {
        comicsWithId.add(it.getComicWithCharacterId(characterId))
    }
    return comicsWithId
}

fun List<EventSummary>.getEventListWithId(characterId: Int): List<EventSummary> {
    val eventsWithId = mutableListOf<EventSummary>()
    this.forEach {
        eventsWithId.add(it.getEventWithCharacterId(characterId))
    }
    return eventsWithId
}