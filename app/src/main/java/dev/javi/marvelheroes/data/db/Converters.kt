package dev.javi.marvelheroes.data.db

import androidx.room.TypeConverter
import dev.javi.marvelheroes.models.Image

/**
 * Converters for room database.
 */
class Converters {

    @TypeConverter
    fun fromImage(value: Image): String {
        return "${value.path}.${value.extension}"
    }

    @TypeConverter
    fun toImage(value: String): Image {
        return Image(value, "")
    }
}