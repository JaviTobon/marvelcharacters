package dev.javi.marvelheroes.data.db

import android.content.Context
import androidx.room.*
import dev.javi.marvelheroes.models.CharacterDatabase
import dev.javi.marvelheroes.models.ComicSummary
import dev.javi.marvelheroes.models.EventSummary

/**
 * Marvel Database
 *
 * This class handles the logic to create database singletons
 *
 */
@Database(entities = [CharacterDatabase::class, ComicSummary::class, EventSummary::class], version = 4, exportSchema = false)
@TypeConverters(Converters::class)
abstract class MarvelDatabase  : RoomDatabase(){

    abstract fun marvelDao(): MarvelDao

    companion object {

        @Volatile
        private var INSTANCE: MarvelDatabase? = null

        fun getInstance(context: Context): MarvelDatabase =
            INSTANCE ?: synchronized(this) {
                INSTANCE
                    ?: buildDatabase(context).also { INSTANCE = it }
            }

        private fun buildDatabase(context: Context) =
            Room.databaseBuilder(context.applicationContext, MarvelDatabase::class.java, "Marvel.db")
                .fallbackToDestructiveMigration()
                .build()
    }
}