package dev.javi.marvelheroes.data

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.paging.PagedList
import dev.javi.marvelheroes.data.db.MarvelLocalCache
import dev.javi.marvelheroes.models.Character
import dev.javi.marvelheroes.models.CharacterDatabase
import dev.javi.marvelheroes.data.network.MarvelApi
import dev.javi.marvelheroes.data.network.fetchCharacters
import dev.javi.marvelheroes.getComicListWithId
import dev.javi.marvelheroes.getEventListWithId
import dev.javi.marvelheroes.toCharacterDatabaseList

class MarvelCallback(private val name: String?,
                     private val api: MarvelApi,
                     private val cache: MarvelLocalCache) : PagedList.BoundaryCallback<CharacterDatabase>() {

    private var lastRequestedPage = 0

    private var isRequestInProgress = false

    val loading: MutableLiveData<Boolean> = MutableLiveData()
    val attributionText: MutableLiveData<String> = MutableLiveData()

    private val _networkErrors = MutableLiveData<String>()

    val networkErrors: LiveData<String>
        get() = _networkErrors

    override fun onZeroItemsLoaded() {
        Log.d("Callback", "onZeroItemsLoaded")
        loading.postValue(true)
        requestAndSaveData(name)
    }

    override fun onItemAtEndLoaded(itemAtEnd: CharacterDatabase) {
        Log.d("Callback", "onItemAtEndLoaded")
        requestAndSaveData(name)
    }

    private fun requestAndSaveData(name: String?){
        if(isRequestInProgress) return

        isRequestInProgress = true
        val startPosition = lastRequestedPage * NETWORK_PAGE_SIZE

        fetchCharacters(api, name, startPosition, NETWORK_PAGE_SIZE, { characterDataWrapper ->
            val charactersDatabase = characterDataWrapper.data.results.toCharacterDatabaseList()
            cache.insertCharacters(charactersDatabase) {
                saveComicsAndEvents(characterDataWrapper.data.results)
                loading.postValue(false)
                attributionText.postValue(characterDataWrapper.attributionText)
            }
        }, { error ->
            Log.d("MarvelDataSource", error)
            isRequestInProgress = false
            loading.postValue(false)
            _networkErrors.postValue(error)
            requestAndSaveData(name)
        })
    }

    private fun saveComicsAndEvents(characters: List<Character>){
        characters.forEach {
            cache.insertComics(it.comics.items.getComicListWithId(it.id)){}
            cache.insertEvents(it.events.items.getEventListWithId(it.id)){}
        }
        lastRequestedPage++
        isRequestInProgress = false
    }

    companion object {
        private const val NETWORK_PAGE_SIZE = 50
    }
}