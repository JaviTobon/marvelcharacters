package dev.javi.marvelheroes.data

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import dev.javi.marvelheroes.data.db.MarvelLocalCache
import dev.javi.marvelheroes.models.*
import dev.javi.marvelheroes.data.network.MarvelApi

class MarvelRepository(
    private val marvelApi: MarvelApi,
    private val cache: MarvelLocalCache
){
    fun searchCharacters(name: String?): CharacterSearchResult {
        Log.d("MarvelRepository", "New search: $name")
        clearTables()

        val dataSourceFactory =
            if(name == null)
                cache.getCharacters()
            else
                cache.getCharactersByName(nameWithWildcard(name))

        val boundaryCallback = MarvelCallback(name, marvelApi, cache)
        val networkErrors = boundaryCallback.networkErrors
        val loading = boundaryCallback.loading
        val attributionText = boundaryCallback.attributionText

        val pagedListConfig = createPagedConfig()

        val data = LivePagedListBuilder(dataSourceFactory,pagedListConfig)
            .setBoundaryCallback(boundaryCallback)
            .build()

        return CharacterSearchResult(data, networkErrors, loading, attributionText)
    }

    private fun clearTables(){
        cache.clearCharacterTable()
        cache.clearComicsTable()
        cache.clearEventsTable()
    }

    private fun createPagedConfig() = PagedList.Config.Builder()
        .setEnablePlaceholders(true)
        .setPageSize(PAGE_SIZE)
        .setMaxSize(MAX_SIZE)
        .setPrefetchDistance(PREFETCH_DISTANCE)
        .build()

    private fun nameWithWildcard(name: String) = "$name%"

    fun searchComics(characterId: Int): LiveData<List<ComicSummary>> = cache.getComicsForCharacter(characterId)

    fun searchEvents(characterId: Int): LiveData<List<EventSummary>> = cache.getEventsForCharacter(characterId)

    fun searchCharacterDetails(characterId: Int): LiveData<CharacterDatabase> = cache.getCharacterDetails(characterId)

    companion object {
        private const val PAGE_SIZE = 20
        private const val MAX_SIZE = 200
        private const val PREFETCH_DISTANCE = 50
    }
}
